# SSSSG - Stupidly Simple Static Site Generator

## TO-DO
- Documentation

## Usage
```ssssg <project_directory>```
`project_directory` defaults to `os.getcwd()`

## Contributing

### Commit messages
Commit messages follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) guidelines.

Summary:
```text
<type>[(<scope>)][!]: <description>
[body]
[footer]
```
Messages with `!` after the scope, or `BREAKING CHANGE:` in the footer are considered a breaking change and will bump the major version on merge into the main branch.

This project uses the following types:
* `fix` - Adjustment of an existing feature
* `feat` - Addition of a feature
* `ci` - Commits made by a bot; e.g. git tags, changelog
* `docs` - Documentation changes
* `tests` - Testing changes


This project uses the following scopes:
* `api` - Changes to the API

Additional scopes may be added as needed.