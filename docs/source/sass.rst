.. _Sass:

SASS Compilation
================

ssssg allows you automatically compile Sass into CSS. Set `SASS_ENABLED` in your `site.toml`:

.. code-block:: TOML

    [site]
    SITE_NAME = "My Site"
    SASS_ENABLED = true

Place your sass files in the `sass` directory and run ssssg:

.. code-block:: sass

    $font-stack: Helvetica, sans-serif;
    $primary-color: #333;
    
    body {
      font: 100% $font-stack;
      color: $primary-color;
    }


.. code-block:: console

    ssssg

The CSS will be output to `output/static/css` by default. See :ref:`Configuration` for more options.