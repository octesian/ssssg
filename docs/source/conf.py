# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from sphinx_pyproject import SphinxConfig

config = SphinxConfig(pyproject_file="../../pyproject.toml")
project = config.name

sys.path.insert(0, os.path.abspath("."))
sys.path.insert(1, os.path.abspath("../../"))
sys.path.insert(2, os.path.abspath("../../src/"))


extensions = [
    "sphinx_rtd_dark_mode",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
]
html_theme = "sphinx_rtd_theme"
default_dark_mode = False
exclude_patterns = []