.. _Configuration:

Configuration
=============

Configuration is handled through `site.toml` in the root directory of the project.  
All configuration is available to templates through the variable `config`.

SITE_NAME
    The name of the website to render.

    default: "My Site"

SITE_URL
    The root URL of the site. If it is an http address, it must end with a "/".

    default: "/"

SASS_ENABLED
    Enable sass CSS compilation.

    default: True

MARKDOWN_EXTENSION
    Extensian of markdown files to render using `MARKDOWN_TEMPLATE`.

    default: ".md"

MARKDOWN_TEMPLATE
    Name of the template to use to render markdown. Must be located in the directory
    defined by the `TEMPLATE_DIR` option.

    default: "markdown.j2"

IGNORE_FILES
    List of files to ignore during rendering.

    default: []

ENCODING
    The encoding to use when writing output.

    default: "utf-8"

SASS_DIR
    The source directory for CSS compilation.

    default: "sass"

STATIC_DIR
    The source directory for static files.

    default: "static"

STATIC_OUTPUT_DIR
    The output directory for static files.

    default: "static"

OUTPUT_DIR
    The output directory for the project.
    It is recommended that you add this to your .gitignore file.

    default: "output"

CSS_OUTPUT_DIR
    The output directory name for compiled css.
    This will be created in the directory OUTPUT_DIR/STATIC_DIR.

    default: "css"

TEMPLATES_DIR
    The directory containing templates to use for markdown rendering.

    default: "templates"

CONTENT_DIR
    The directory containing your website content.

    default: "content"

IGNORE_DIRS
    List of directories to ignore during rendering.

    default: [".git", "venv"]

SITEMAP_STRUCTURE
    Decides how to categorize items in the sitemap. "directory_structure" will 
    create a dict object modeled after the directory structure in your "CONTENT_DIR".
    "parent_folder" will group all pages with the same parent folder name.
    all items in the same folder.

    choices: "directory_structure", "parent_folder"
    
    default: "directory_structure"    


Template Variables
==================

In addition to all the options in "site.toml" being available to Jinja templates, the following
variables are also available:

SITEMAP
    A structured list of rendered files based on the directory structure in the CONTENT_DIR.
    The structure 