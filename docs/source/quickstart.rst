.. _Quickstart:

Quickstart
============

Create `site.toml` and add the site section and change the `SITE_NAME` option.
Disable SASS for now. See :ref:`Sass` for more information.

.. code-block:: toml

    [site]
    SITE_NAME = "My awesome site"
    SASS_ENABLED = false

Add the `markdown.j2` template to the `templates` directory:

.. code-block:: HTML

    <html>
      <head>
          <meta charset="utf-8" />
          <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
        <title>
            {{ SITE_NAME }}
        </title>
      </head>
      <body>
        {{ md_content | to_markdown }}
      </body>
    </html>

Add a page to the `content` directory:

.. code-block:: Markdown

    # First blog post
    ---
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
    consequat.


Run ssssg. The output will be in the `output` directory.

.. code-block:: console

    ssssg


Run ssssg with a development server to see real time changes as you make them:

.. code-block:: console

    ssssg --dev