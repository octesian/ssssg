.. ssssg documentation master file, created by
   sphinx-quickstart on Mon Feb 28 18:07:15 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Stupidly Simple Static Site Generator
=====================================
.. toctree::
   :maxdepth: 2
   
   installation
   quickstart
   configuration
   sass
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
