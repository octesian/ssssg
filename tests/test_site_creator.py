from unittest import TestCase
from ssssg.site_creator import SiteConfig, SiteCreator, DevHandler
from os import chdir, getcwd
from os.path import join, dirname, pardir, abspath, isdir, isabs


TEST_DIR = abspath(dirname(__file__))
TEST_PROJECT_DIR = join(TEST_DIR, "test_project")


class TestSiteConfig(TestCase):
    def test___init__(self) -> None:
        jc = SiteConfig(TEST_PROJECT_DIR)
        for key, value in jc.config.items():
            self.assertIsNotNone(jc.config[key])

    def test_read_config_file(self) -> None:
        jc = SiteConfig(TEST_PROJECT_DIR)
        d = jc.read_config_file()
        assert type(d) == dict

    def test__absolute_paths(self) -> None:
        jc = SiteConfig(TEST_PROJECT_DIR)
        d = jc.read_config_file()
        self.assertTrue(isabs(jc.config["STATIC_DIR"]))


class TestSiteCreator(TestCase):
    def setUp(self) -> None:
        self.sc = SiteCreator(TEST_PROJECT_DIR)

    def test___init__(self) -> None:
        pass
    
    def test__add_menu_item_parent(self) -> None:
        rel_path = "/some/nested/path"
        base_name = "file"
        menu_item = self.sc._add_sitemap_parent(rel_path, base_name)
        self.assertIsInstance(self.sc.config["SITEMAP"]["path"]["items"][0], tuple)
        rel_path = "."
        base_name = "file"
        menu_item = self.sc._add_sitemap_parent(rel_path, base_name)
        print(self.sc.config["SITEMAP"])
        self.assertEqual(len(self.sc.config["SITEMAP"]["items"]), 1)
        rel_path = "/"
        base_name = "file2"
        menu_item = self.sc._add_sitemap_parent(rel_path, base_name)
        print(self.sc.config["SITEMAP"])
        self.assertEqual(len(self.sc.config["SITEMAP"]["items"]), 2)
    
    def test__add_menu_item_directory(self) -> None:
        rel_path = "/some/nested/path"
        base_name = "file"
        menu_item = self.sc._add_sitemap_directory(rel_path, base_name)
        self.assertIsInstance(self.sc.config["SITEMAP"]["some"]["nested"]["path"]["items"][0], tuple)
        rel_path = "/some"
        base_name = "file"
        menu_item = self.sc._add_sitemap_directory(rel_path, base_name)
        self.assertIsInstance(self.sc.config["SITEMAP"]["some"]["items"][0], tuple)
        self.assertIsInstance(self.sc.config["SITEMAP"]["some"]["nested"]["path"]["items"][0], tuple)

    def test_discover(self) -> None:
        pages = self.sc.discover()
        self.assertIsInstance(pages, list)
        self.assertEqual(len(pages), 4)

    def test_copy_static(self) -> None:
        static = self.sc.copy_static()
        self.assertTrue(isdir(static))

    def test_create_page(self) -> None:
        pass

    def test_compile_sass(self) -> None:
        pass

    def test_create_site(self) -> None:
        pass

    def start_dev_server(self) -> None:
        pass


class TestDevHandler:
    def test___init__(self) -> None:
        pass
