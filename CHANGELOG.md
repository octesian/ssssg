<!--next-version-placeholder-->

## v3.1.1 (2022-12-12)
### Fix
* Fix page_var pollution ([`46077a8`](https://gitlab.com/octesian/ssssg/-/commit/46077a849e2641c32fe6c08ec3f269eba7cab0ef))

## v3.1.0 (2022-12-12)
### Feature
* Add SITEMAP_STRUCTURE config option ([`56f05cd`](https://gitlab.com/octesian/ssssg/-/commit/56f05cd8fb027cc4299783030a82fd8dc7f4dc51))

## v3.0.2 (2022-12-11)
### Fix
* Fix issue with default config not being overwritten ([`d7671b0`](https://gitlab.com/octesian/ssssg/-/commit/d7671b05b83efdd93f16c46b734ce4831ca823c7))

## v3.0.1 (2022-12-11)


## v3.0.0 (2022-12-11)
### Fix
* Replace os.path with pathlib ([`56bf27c`](https://gitlab.com/octesian/ssssg/-/commit/56bf27c16e9822c17751156ffaaf7820a946f4c2))
* Replace ConfigParser with tomllib ([`0b8b047`](https://gitlab.com/octesian/ssssg/-/commit/0b8b047a0f6b1759cd5f9af377bf93b7f1797941))

### Breaking
* tomllib is only available from python 3.11+  ([`0b8b047`](https://gitlab.com/octesian/ssssg/-/commit/0b8b047a0f6b1759cd5f9af377bf93b7f1797941))

### Documentation
* Add docs job ([`4348dea`](https://gitlab.com/octesian/ssssg/-/commit/4348deaa08300a7846642344c139ea9e50b89dd1))
* Add API documentation ([`8275143`](https://gitlab.com/octesian/ssssg/-/commit/8275143366c2eb673915ae84d1749a633e8ec3bc))
* Add default autoapi templates ([`fa0b4fe`](https://gitlab.com/octesian/ssssg/-/commit/fa0b4fefa85f2c33c364c1eb48add470d3229082))

## v2.1.11 (2022-10-31)


## v2.1.10 (2022-10-12)

# 1.0.2
- meta: Auto-versioning